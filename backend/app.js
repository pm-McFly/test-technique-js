const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const port = 3000;
const leadersRouter = require('./routes/leaderboard');
const processRouter = require('./routes/process');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.get('/', (req, res) => res.send('Hello World!'));
app.get('/process', processRouter);
app.use('/leaderboard', leadersRouter);

app.listen(port, () => console.log(`Example app listening on port ${port}!`));