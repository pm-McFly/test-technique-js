let Data = {
    options: {
        "NULL": "-- SELECT ONE --",
        "de-DE": "Dutch - Germany",
        "en-GB": "English - Great Britain",
        "en-US": "English - United States",
        "es-ES": "Spanish - Spain",
        "es-LA": "Spanish - Latin America",
        "es-US": "Spanish - United States",
        "fr-FR": "French - France",
        "it-IT": "Italian - Italy",
        "ja-JP": "Japanese - Nippon",
        "pt-BR": "Portuguese - Portugal",
    },
    turns: {
        "0": "-- SELECT ONE --",
        "1": "1",
        "2": "2",
        "3": "3",
        "4": "4",
        "5": "5",
        "6": "6",
        "7": "7",
        "8": "8",
        "9": "9",
        "10": "10",
    }
};

module.exports = Data;