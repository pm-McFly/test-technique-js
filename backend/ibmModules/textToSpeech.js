const TextToSpeechV1 = require('ibm-watson/text-to-speech/v1');
const fs = require('fs');

const langArray = [
    'de-DE_BirgitV3Voice',
    'de-DE_DieterV3Voice',
    'en-GB_KateV3Voice',
    'en-US_AllisonV3Voice',
    'en-US_LisaV3Voice',
    'en-US_MichaelV3Voice',
    'es-ES_EnriqueV3Voice',
    'es-ES_LauraV3Voice',
    'es-LA_SofiaV3Voice',
    'es-US_SofiaV3Voice',
    'fr-FR_ReneeV3Voice',
    'it-IT_FrancescaV3Voice',
    'ja-JP_EmiV3Voice',
    'pt-BR_IsabelaV3Voice'
];

function randomIntInc(low, high) {
    return Math.floor(Math.random() * (high - low + 1) + low)
}

function getRandomLang() {
    return langArray[randomIntInc(0, langArray.length - 1)];
}

const textToSpeech = new TextToSpeechV1({
    iam_apikey: 'qZ79uQVETXEOKghUDCUaCYQLZz_YaImkhJ4zCr8y1l_5',
    url: 'https://gateway-lon.watsonplatform.net/text-to-speech/api',
    disable_ssl_verification: true
});

async function setSynthesizeParams(interSent, lang) {
    return {
        text: interSent[interSent.length - 1],
        accept: 'audio/wav',
        voice: lang,
    }
}

async function synthText(interSent, actLang, fileName, ibmFunc) {
    let synthesizeParams = await setSynthesizeParams(interSent, actLang);
    await ibmFunc.synthesize(synthesizeParams)
        .then(async audio => {
            await audio.pipe(await fs.createWriteStream(fileName));
        })
        .catch(err => {
            console.log('error:', err);
        })
}

module.exports = {
    textToSpeech,
    synthText,
    getRandomLang
};